import './App.css';
import './fonts.css';

import item1 from './img/item-1.png';
import item2 from './img/item-2.png';
import item3 from './img/item-3.png';
import item4 from './img/item-4.png';
import item5 from './img/item-5.png';

import course1 from './img/course-1.png';
import course2 from './img/course-2.png';
import course3 from './img/course-3.png';
import course4 from './img/course-4.png';

import prev from './img/prev.png';
import next from './img/next.png';
import partner1 from './img/partner-1.png';
import partner2 from './img/partner-2.png';
import partner3 from './img/partner-3.png';
import partner4 from './img/partner-4.png';

import team1 from './img/team-1.jpg';
import team2 from './img/team-2.jpg';
import team3 from './img/team-3.jpg';
import team4 from './img/team-4.jpg';
import fb_ico from './img/social - facebook - 1.png';
import tw_ico from './img/social - twitter - 1.png';
import gp_ico from './img/social - google - plus.png';

import fb_ico_blue from './img/footer-facebook-blue.png';
import fb_ico_red from './img/footer-facebook-red.png';
import tw_ico_blue from './img/footer-twitter-blue.png';
import tw_ico_red from './img/footer-twitter-red.png';
import gp_ico_blue from './img/footer-google-plus-blue.png';
import gp_ico_red from './img/footer-google-plus-red.png';


const App = () => {
    return (
        <div className="App">
            <Header/>
            <Features/>
            <Courses/>
            <Partners/>
            <Promo/>
            <OurTeam/>
            <Form/>
            <Footer/>
        </div>
    );
}

function Header() {
    return (
        <header>
            <div className="container">
                <div className="row">
                    <div className="col-3 align-self-end logo">
                        <a href="#" className="">COURSEWAY</a>
                    </div>
                    <div className="col-8 ml-auto align-self-end nav-list">
                        <nav>
                            <ul className="menu">
                                <li className="menu-item">
                                    <div className="active"></div>
                                    <a href="#">головна</a></li>
                                <li className="menu-item"><a href="#">курси</a></li>
                                <li className="menu-item"><a href="#">блог</a></li>
                                <li className="menu-item"><a href="#">про проект</a></li>
                                <li className="menu-item"><a href="#">реєстрація</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="row">
                    <div className="col-11 header-title">
                        <h1>БЕЗКОШНОВНІ ОНЛАЙН-КУРСИ</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-11 header-text">
                        <p>ВІД ВИКЛАДАЧІВ ПРОВІДНИХ УНІВЕРСИТЕТІВ СВІТУ</p>
                    </div>
                </div>
                <div className="row form">
                    <div className="col-9">
                        <input type="text" placeholder="Email:"></input>
                        <br></br>
                        <input type="text" placeholder="Password:"></input>
                        <br></br>
                        <button>РОЗПОЧАТИ!</button>
                    </div>
                </div>
            </div>
        </header>
    );
}

function Features() {
    return (
        <section className="features">
            <div className="container">
                <h2 className="body-tittle text-center">ПЕРЕВАГИ</h2>
                <div className="row">
                    <div className="col-4 text-center ">
                        <div className="short-div text-center align-self-start">
                            <img src={item1} alt=""></img>
                            <h3 className="item-title">Будь-коли</h3>
                            <p className="item-text">Навчайтесь у зручний для Вас час. Відеолекції, тести та форум
                                доступні
                                цілодобово.</p>
                        </div>
                        <div className="short-div text-center align-self-end">
                            <img src={item2} alt=""></img>
                            <h3 className="item-title">Будь-де</h3>
                            <p className="item-text">Проходьте курси, не виходячи з дому чи в дорозі. Все, що Вам
                                знадобиться, – це
                                доступ до інтернету.</p>
                        </div>
                    </div>
                    <div className="col-4 align-self-center text-center">
                        <img src={item5} alt=""></img>
                    </div>
                    <div className="col-4 text-center">
                        <div className="short-div text-center align-self-start">
                            <img src={item3} alt=""></img>
                            <h3 className="item-title">Будь-який пристрій</h3>
                            <p className="item-text">Навчайтесь на комп’ютері, планшеті чи телефоні: сайт
                                підлаштується під Ваш
                                пристрій.</p>
                        </div>
                        <div className="short-div text-center align-self-end">
                            <img src={item4} alt=""></img>
                            <h3 className="item-title">Будь-хто</h3>
                            <p className="item-text">Наші курси абсолютно безкоштовні – захмарні ціни більше ніколи
                                не стануть на
                                заваді
                                найкращій освіті.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

function Courses() {
    return (
        <section className="courses">
            <div className="container">
                <h2 className="body-tittle">Доступні курси</h2>
                <p className="body-text">Запишіться прямо зараз на безкоштовні курси</p>
                <div className="row justify-content-center">
                    <div className="row course-item">
                        <div className="col-6 course-item-tblock">

                            <h3 className="course-item-title">Основи програмування</h3>
                            <p className="course-item-text">After the first 50 completed projects to make a mistake with
                                the
                                deadlines almost unreal.</p>
                            <span className="read-more"><a href="#">Read more...</a></span>

                            <div className="row justify-content-end align-self-end">
                                <button className="button-reg">ЗАРЕЄСТРУВАТИСЬ НА КУРСІ</button>
                            </div>

                        </div>

                        <div className="col-6 course-item-img">
                            <img src={course1} alt=""></img>
                        </div>

                    </div>
                    <div className="row course-item">
                        <div className="col-6 course-item-tblock">
                            <h3 className="course-item-title">Розробка та аналіз алгоритмів</h3>
                            <p className="course-item-text">After the first 50 completed projects to make a mistake with
                                the
                                deadlines almost unreal.</p>
                            <span className="read-more"><a href="#">Read more...</a></span>
                            <div className="row justify-content-end align-self-end">
                                <button className="button-reg">ЗАРЕЄСТРУВАТИСЬ НА КУРСІ</button>
                            </div>

                        </div>
                        <div className="col-6">
                            <img src={course2} alt=""></img>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="row course-item">
                        <div className="col-6 course-item-tblock">

                            <h3 className="course-item-title mb-0">Економіка</h3>
                            <h3 className="course-item-title">для всіх</h3>
                            <p className="course-item-text">After the first 50 completed projects to make a mistake with
                                the
                                deadlines almost unreal.</p>
                            <span className="read-more"><a href="#">Read more...</a></span>

                            <div className="row justify-content-end align-self-end">
                                <button className="button-reg">ЗАРЕЄСТРУВАТИСЬ НА КУРСІ</button>
                            </div>

                        </div>

                        <div className="col-6">
                            <img src={course3} alt=""></img>
                        </div>

                    </div>
                    <div className="row course-item">
                        <div className="course-item-tblock col-6">
                            <h3 className="course-item-title mb-0">Історія</h3>
                            <h3 className="course-item-title text-center">України</h3>
                            <p className="course-item-text">After the first 50 completed projects to make a mistake with
                                the
                                deadlines almost unreal.</p>
                            <span className="read-more"><a href="#">Read more...</a></span>

                            <div className="row justify-content-end align-self-end">
                                <button className="button-reg">ЗАРЕЄСТРУВАТИСЬ НА КУРСІ</button>
                            </div>

                        </div>
                        <div className="col-6">
                            <img src={course4} alt=""></img>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

function Partners() {
    return (
        <section className="partners">
            <div className="container-fluid d-flex justify-content-center">
                <div className="partner-prev align-self-center justify-content-end">
                    <img src={prev} alt=""></img>
                </div>
                <div className="container partner-block">
                    <div className="row justify-content-center">
                        <div className="col-9 text-center">
                            <h2 className="body-tittle">ПАРТНЕРИ - ОСВІТНІ ЗАКЛАДИ</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3 partner-item text-center">
                            <img src={partner1} alt=""></img>
                            <h3 className="partner-name text-center">Cambridge</h3>
                        </div>
                        <div className="col-3 partner-item text-center">
                            <img src={partner2} alt=""></img>
                            <h3 className="partner-name red text-center">Harvard</h3>
                        </div>
                        <div className="col-3 partner-item text-center">
                            <img src={partner3} alt=""></img>
                            <h3 className="partner-name text-center">MIT</h3>
                        </div>
                        <div className="col-3 partner-item text-center">
                            <img src={partner4} alt=""></img>
                            <h3 className="partner-name text-center">UCLA</h3>
                        </div>
                    </div>
                </div>
                <div className="partner-next align-self-center">
                    <img src={next} alt=""></img>
                </div>
            </div>
        </section>
    );
}

function Promo() {
    return (
        <section className="promo">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-6 promo-img"></div>
                    <div className="col-6 promo-block">
                        <h2 className="promo-title">СЕРТИФІКАТ ВІД ГАРВАРДУ</h2>
                        <p className="promo-text">Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry.
                            Lorem
                            Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                            printer took
                            a
                            galley of type.</p>
                        <button className="promo-button">ОТРИМАТИ!</button>
                    </div>
                </div>
            </div>
        </section>
    );
}

function OurTeam() {
    return (
        <section className="our-team">
            <div className="container">
                <h2 className="body-tittle">Розробники</h2>
                <div className="row justify-align-center">
                    <div className="col-3 team-item text-center">
                        <img className="team-img" src={team1} alt=""></img>
                        <h3 className="team-name">Daniel Raynolds</h3>
                        <p className="team-text">After the first 50 completed projects to make a mistake with the
                            deadlines almost
                            unreal. After the first hundred projects to break the promises already impossible.</p>
                        <div className="row justify-content-center">
                            <div className="col-6 icons">
                                <a href="#"><img src={fb_ico} alt=""></img></a>
                                <a href="#"><img src={tw_ico} alt=""></img></a>
                                <a href="#"><img src={gp_ico} alt=""></img></a>
                            </div>
                        </div>
                    </div>
                    <div className="col-3 team-item text-center">
                        <img className="team-img" src={team2} alt=""></img>
                        <h3 className="team-name">Nick Parson</h3>
                        <p className="team-text">After the first 50 completed projects to make a mistake with the
                            deadlines almost
                            unreal. After the first hundred projects to break the promises already impossible.</p>
                        <div className="row justify-content-center">
                            <div className="col-6 icons">
                                <a href="#"><img src={fb_ico} alt=""></img></a>
                                <a href="#"><img src={tw_ico} alt=""></img></a>
                                <a href="#"><img src={gp_ico} alt=""></img></a>
                            </div>
                        </div>
                    </div>
                    <div className="col-3 team-item text-center">
                        <img className="team-img" src={team3} alt=""></img>
                        <h3 className="team-name">William Stanley</h3>
                        <p className="team-text">After the first 50 completed projects to make a mistake with the
                            deadlines almost
                            unreal. After the first hundred projects to break the promises already impossible.</p>
                        <div className="row justify-content-center">
                            <div className="col-6 icons">
                                <a href="#"><img src={fb_ico} alt=""></img></a>
                                <a href="#"><img src={tw_ico} alt=""></img></a>
                                <a href="#"><img src={gp_ico} alt=""></img></a>
                            </div>
                        </div>
                    </div>
                    <div className="col-3 team-item text-center">
                        <img className="team-img" src={team4} alt=""></img>
                        <h3 className="team-name">Sarah Noel</h3>
                        <p className="team-text">After the first 50 completed projects to make a mistake with the
                            deadlines almost
                            unreal. After the first hundred projects to break the promises already impossible.</p>
                        <div className="row justify-content-center">
                            <div className="col-6 icons">
                                <a href="#"><img src={fb_ico} alt=""></img></a>
                                <a href="#"><img src={tw_ico} alt=""></img></a>
                                <a href="#"><img src={gp_ico} alt=""></img></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

function Form() {
    return (
        <section className="form">
            <div className="container-fluid text-center">
                <h1 className="reg-title">ЗАРЕЄСТРУЙСЯ ЗАРАЗ</h1>
                <p className="reg-text">Та отримай задоволення від навчання</p>
                <input placeholder="Name:"></input>
                <br></br>
                <input placeholder="Email:"></input>
                <br></br>
                <button>Submit</button>
            </div>
        </section>
    );
}
function Footer() {
    return (
        <footer>
            <div className="container-fluid text-center">
                <p>Made with love by <span className="red">Alex Kuzmenko</span></p>
                <p>Inspired by <span className="red">PROMETHEUS</span></p>
                <div className="row justify-content-center c1">
                    <div className="col-6 icons">
                        <a href="#"><img src={tw_ico_blue} alt=""></img></a>
                        <a href="#"><img src={fb_ico_blue} alt=""></img></a>
                        <a href="#"><img src={gp_ico_blue} alt=""></img></a>
                    </div>
                </div>
                <div className="row justify-content-center c2">
                    <div className="col-6 icons">
                        <a href="#"><img src={tw_ico_red} alt=""></img></a>
                        <a href="#"><img src={fb_ico_red} alt=""></img></a>
                        <a href="#"><img src={gp_ico_red} alt=""></img></a>
                    </div>
                </div>
            </div>
        </footer>
);
}

export default App;
